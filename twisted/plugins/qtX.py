
# Copyright (c) 2001-2010 Twisted Matrix Laboratories.
# see LICENSE for details


from twisted.application.reactors import Reactor

qtX = Reactor('qtX', 'qtXreactor', 'PyQt5, PySide2 and PySide integration reactor')
